# How to Use These
The scipts in this folder will be listed as follows along with better documentation into how they can be used, and possibly how they work.  
PyMISP documentation page: https://pymisp.readthedocs.io/en/latest/

### Worth mentioning before beginning
The "restSearch" section in the MISP events section of MISP has templates for scripts similar to these.
If you'd like to search for information, but don't quite know how to make the search, go to the "restSearch" tab, pick the closest template, change at least manditory variables, run, and view the options below. 
If you prefer to use curl for API queries, use that; if you prefer pyMISP, then use that.
Those scripts can then be further modified to better suit your python/bash needs afterward.

For anyone newer to scripting: Keep in mind that you can call other scripts in scripts. So if you can combine their utilization, such as pulling a pymisp query from the template, then running that within another script, it may help save some time, as we don't need to learn pyMISP as deeply anymore. This is one option, it may or may not be ideal for what you are making. I trust you'll make the best choice for your circumstance. ;)

## Prerequisits
Copy the [keys.py.sample](../keys.py.sample) to the directory you are working in, and rename it to "keys.py".  
You will then want to edit the existing variables according to what you need.  
Example:
`
misp_url = 'https://192.168.0.3/' # https://misp.local for domain example
misp_key = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' # The MISP auth key can be found on the MISP web interface under the automation section
misp_verifycert = False
misp_client_cert = ''
proofpoint_key = 'Your Proofpoint TAP auth key'
`  
These variables get imported into most if not all scripts used.

### last.py
Easiest one to test with.  
*Note:* `jq` is a terminal app for viewing json data easier, thus is recommended so you can view your test data easier.  
Examples:
`Usage for pipe masters: ./last.py -l 5h | jq . `
`Usage in case of large data set and pivoting page by page: python3 last.py  -l 48h  -m 10 -p 2  | jq .[].Event.info `

This "last.py" has the `--tag` and `--org` option added for our ease of use.

### searches.py
Searches is a bit more flexible, but more complex usage.  
A search will look like this: `python3 search.py -p org -s ORGNAME | jq`  
Create a search by determining what parameter you want to search for (-p) and then add how your search is spelled/partially spelled (-s).
The parameters you can use are here: https://pymisp.readthedocs.io/en/latest/modules.html?highlight=search#pymisp.PyMISP.search