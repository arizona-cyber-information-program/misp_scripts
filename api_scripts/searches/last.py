#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymisp import ExpandedPyMISP
from keys import misp_url, misp_key, misp_verifycert
try:
    from keys import misp_client_cert
except ImportError:
    misp_client_cert = ''
import argparse
import os


# Usage for pipe masters: ./last.py -l 5h | jq .
# Usage in case of large data set and pivoting page by page: python3 last.py  -l 48h  -m 10 -p 2  | jq .[].Event.info
# Example: python3 last_by_org.py -l 7d --org "ORGNAME"

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Download latest events from a MISP instance.')
    parser.add_argument("-l", "--last", required=True, help="can be defined in days, hours, minutes (for example 5d or 12h or 30m).")
    parser.add_argument("-m", "--limit", required=False, default="10", help="Add the limit of records to get (by default, the limit is set to 10)")
    parser.add_argument("-p", "--page", required=False, default="1", help="Add the page to request to paginate over large dataset (by default page is set to 1)")
    parser.add_argument("-o", "--output", help="Output file")
    parser.add_argument("--org", required=False, help="Name of the organization to search for.")
    parser.add_argument("-t","--tag", required=False, help="Name of the tag to search for.")

    args = parser.parse_args()

    if args.output is not None and os.path.exists(args.output):
        print('Output file already exists, aborted.')
        exit(0)

    if misp_client_cert == '':
        misp_client_cert = None
    else:
        misp_client_cert = (misp_client_cert)

    misp = ExpandedPyMISP(misp_url, misp_key, misp_verifycert, cert=misp_client_cert)
    result = misp.search(orgs=args.org, tags=args.tag, publish_timestamp=args.last, limit=args.limit, page=args.page, pythonify=True)

    if not result:
        print('No results for that time period')
        exit(0)

    if args.output:
        with open(args.output, 'w') as f:
            for r in result:
                f.write(r.to_json() + '\n')
    else:
        for r in result:
            print(r.to_json())
