#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymisp import PyMISP
from keys import misp_url, misp_key,misp_verifycert
import argparse
import os
import json


def init(url, key):
    return PyMISP(url, key, misp_verifycert, 'json')


def search(mispObj, quiet, url, controller, out=None, **kwargs):
    results = mispObj.search(controller, pythonify=False, **kwargs)
    if quiet:
        for result in results:
            print('{}{}{}\n'.format(url, '/events/view/', result))
    elif out is None:
        for result in results:
            print(result.to_json())
    else:
        with open(out, 'w') as outputFile:
            for result in results:
                outputFile.write(print(result.to_json()))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Get all the events matching a value for a given param.')
    parser.add_argument("-p", "--param", required=True, help="Parameter to search (e.g. category, org, values, type_attribute, etc.)")
    parser.add_argument("-s", "--search", required=True, help="String to search.")
    parser.add_argument("-a", "--attributes", action='store_true', help="Search attributes instead of events")
    parser.add_argument("-q", "--quiet", action='store_true', help="Only display URLs to MISP")
    parser.add_argument("-o", "--output", help="Output file")

    args = parser.parse_args()

    if args.output is not None and os.path.exists(args.output):
        print('Output file already exists, abort.')
        exit(0)

    misp = init(misp_url, misp_key)
    kwargs = {args.param: args.search}

    if args.attributes:
        controller='attributes'
    else:
        controller='events'

    search(misp, args.quiet, misp_url, controller, args.output, **kwargs)
