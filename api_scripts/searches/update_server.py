#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymisp import ExpandedPyMISP
from keys import misp_url, misp_key, misp_verifycert
try:
    from keys import misp_client_cert
except ImportError:
    misp_client_cert = ''

if __name__ == '__main__':
    misp = ExpandedPyMISP(misp_url, misp_key, misp_verifycert, cert=misp_client_cert)
    misp.update_misp()
