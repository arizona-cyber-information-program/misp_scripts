#!/usr/bin/python

# Use this template when creating a new python script

import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Description of utility.')
    parser.add_argument('option', help='What this option does.')
    args = parser.parse_args()
    print(args.some_option)
