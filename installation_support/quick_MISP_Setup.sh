#!/bin/bash

# This is script is tested on Ubuntu, though should work for Debian and Kali, since the install script checks for that.

sudo apt update
sudo apt full-upgrade -y
#sudo apt install -y vim htop iotop bmon
sudo apt autoremove -y

# If the device is particularly slow, the next line may be desireable
#echo "$USER  ALL=(ALL:ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers

wget -O /tmp/INSTALL.sh https://raw.githubusercontent.com/MISP/MISP/2.4/INSTALL/INSTALL.sh
bash /tmp/INSTALL.sh -c -m

